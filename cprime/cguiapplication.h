/*
  *
  * This file is a part of Libcprime.
  * Library for saving activites and bookmarks, share file and more.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This file is derived from QSingleApplication, originally written
  * as a part of Qt Solutions. For license read DesQApplication.cpp
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */

#pragma once

#include <QGuiApplication>
#include <QLockFile>
#include <QLocalServer>
#include <QLocalSocket>

#if defined(qApp)
#undef qApp
#endif

namespace CPrime {
class CGuiApplication;
}

#define qApp    (static_cast<CPrime::CGuiApplication *>(QCoreApplication::instance()))

class CPrime::CGuiApplication : public QGuiApplication {
	Q_OBJECT

public:
	CGuiApplication(const QString& id, int& argc, char **argv);

	~CGuiApplication();

	bool isRunning();
	QString id() const;

public Q_SLOTS:
	bool sendMessage(const QString& message, int timeout = 500);
	void disconnect();

Q_SIGNALS:
	void messageReceived(const QString& message);

private:
	void handleConnection();

	QLockFile *lockFile = nullptr;

	QString mSocketName;
	QString mAppId;

	QLocalServer *mServer;
};
