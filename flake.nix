{
  description = "A flake for building Hello World";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;

  outputs = { self, nixpkgs }: {

    packages.x86_64-linux.default =
      # Notice the reference to nixpkgs here.
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation rec {
  pname = "libcprime";
  version = "4.3.0";

  #src = fetchFromGitLab {
  #  owner = "cubocore";
  #  repo = pname;
  #  rev = "v${version}";
  #  sha256 = "sha256-+z5dXKaV2anN6OLMycEz87kDqQScgHHEKwGhDAdHSd4=";
  #};

  src = ./.;
  dontWrapQtApps = true;

  #patches = [
  #  ./0001-fix-application-dirs.patch
  #];

  cmakeFlags = [ "-DUSE_QT6=ON" ];
  mesonFlags = [ "--buildtype=release  -Duse_qt_version=qt6 -DUSE_QT6=ON" ];

  nativeBuildInputs = [
    cmake
    ninja
  ];

  buildInputs = [
    qt6.qtbase
    qt6.qtconnectivity
    libnotify
  ];
      };

  };
}

