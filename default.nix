(import (fetchTarball { url = "https://github.com/edolstra/flake-compat/archive/master.tar.gz"; sha256 = "1prd9b1xx8c0sfwnyzkspplh30m613j42l1k789s521f4kv4c2z2"; }) {
  src = builtins.fetchGit ./.;
}).defaultNix
